package com.ryhma_9.rego_view;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.ResourceBundle;

import com.ryhma_9.rego.Main;
import com.ryhma_9.rego_util.Tyontekijat;
import com.ryhma_9.rego_util.Tyotehtava;
import com.ryhma_9.rego_util.AccessRegoTietokanta;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class TyontekijatController {
	private Connection connect = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	private boolean resultSet2;
	final private String database = "rego";
	@FXML
	private TextField name;
	@FXML
	private TextField lastname;

	private Stage primaryStage;
	@FXML
	private Label etunimi;
	@FXML
	private Label sukunimi;
	@FXML
	private Label puhelinnumero;
	@FXML
	private Label sahkoposti;
	@FXML
	private Label postiosoite;
	@FXML
	private Label yhteystiedot;
	// ------------------------------ //Kieli käännöstä varten tehdyt muuttujat
	@FXML
	private Label etunimi1;
	@FXML
	private Label sukunimi1;
	@FXML
	private Label puhelinnumero1;
	@FXML
	private Label sahkoposti1;
	@FXML
	private Label postiosoite1;
	@FXML
	private Label yhteystiedot1;
	// ------------------------
	@FXML
	private Button paivita;
	@FXML
	private Button uusityontekija;
	@FXML
	private Button poista;

	@FXML
	private ObservableList<Tyontekijat> tyontekijoidentiedot = FXCollections.observableArrayList();
	@FXML
	private TableColumn<Tyontekijat, String> col_etunimi;

	@FXML
	private TableColumn<Tyontekijat, String> col_sukunimi;
	@FXML
	private TableView<Tyontekijat> table_tt;

	private Stage dialogStage;

	@FXML
	private boolean okClicked = false;

	public String data;

	/**
	 * initialize() metodia kutsutaan kun näkymä käynnistyy, metodi ottaa yhteyttä
	 * tietokantaan ja asettaa arvot taulukkoon
	 */

	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		etunimi1.setText(bundle.getString("etunimi"));
		sukunimi1.setText(bundle.getString("sukunimi"));
		puhelinnumero1.setText(bundle.getString("puhelinnumero"));
		sahkoposti1.setText(bundle.getString("sahkoposti"));
		postiosoite1.setText(bundle.getString("osoite"));
		poista.setText(bundle.getString("button_delete_employee"));
		paivita.setText(bundle.getString("button_refresh"));
		uusityontekija.setText(bundle.getString("button_uusityontekija"));
		col_etunimi.setText(bundle.getString("etunimi"));
		col_sukunimi.setText(bundle.getString("sukunimi"));
		yhteystiedot1.setText(bundle.getString("yhteystiedot"));
		try {
			System.out.println("Nyt olaan TyontekijatControllerin initialize metodissa");
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".omattiedot");
			while (resultSet.next()) {
				tyontekijoidentiedot.add(new Tyontekijat(resultSet.getString("id"), resultSet.getString("etunimi"),
						resultSet.getString("sukunimi"), resultSet.getString("puh"), resultSet.getString("spost"),
						resultSet.getString("posti")));

				col_etunimi.setCellValueFactory(new PropertyValueFactory<Tyontekijat, String>("etunimi"));
				col_sukunimi.setCellValueFactory(new PropertyValueFactory<Tyontekijat, String>("sukunimi"));
				table_tt.setItems(tyontekijoidentiedot);
				System.out.println("Nyt olaan TyontekijatControllerin initialize metodin lopussa");

				table_tt.setOnMousePressed(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {
						if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {

							valituntyontekijantiedot();
							showTyontekijatMuokkaaTyöntekijaaView();
						} else if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
							valituntyontekijantiedot();
							LabelAsetus();

						}
					}

				});
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi joka asettaa valitun työntekijän tiedot taulukkoon
	 */

	@FXML
	void LabelAsetus() {
		try {
			Connection con = AccessRegoTietokanta.getConnection();
			String sql = "select * from rego.omattiedot where etunimi=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, data);
			resultSet = ps.executeQuery();
			if (resultSet.next()) {
				etunimi.setText(resultSet.getString("etunimi"));
				sukunimi.setText(resultSet.getString("sukunimi"));
				puhelinnumero.setText(resultSet.getString("puh"));
				sahkoposti.setText(resultSet.getString("spost"));
				postiosoite.setText(resultSet.getString("posti"));
				System.out.println(
						"Olen TyontekijaControllerin printti ja tarkistan data muuttujan arvon rivi 83 " + data);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi hoitaa taulukon päivittämisen
	 */

	@FXML
	private void handleRefresh() {
		try {
			Connection con3 = AccessRegoTietokanta.getConnection();
			resultSet = con3.createStatement().executeQuery("select * from " + database + ".omattiedot");
			table_tt.getItems().clear();
			while (resultSet.next()) {
				tyontekijoidentiedot.add(new Tyontekijat(resultSet.getString("id"), resultSet.getString("etunimi"),
						resultSet.getString("sukunimi"), resultSet.getString("puh"), resultSet.getString("spost"),
						resultSet.getString("posti")));

				col_etunimi.setCellValueFactory(new PropertyValueFactory<Tyontekijat, String>("etunimi"));
				col_sukunimi.setCellValueFactory(new PropertyValueFactory<Tyontekijat, String>("sukunimi"));
				table_tt.setItems(tyontekijoidentiedot);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi tallentaa tietokantaan data muuttujan joka on valittu työntekijä
	 */

	@FXML
	void valituntyontekijantiedot() {
		int pos = table_tt.getSelectionModel().getSelectedIndex();
		TablePosition poss = table_tt.getSelectionModel().getSelectedCells().get(0);
		int row = poss.getRow();
		Tyontekijat item = table_tt.getItems().get(row);
		TableColumn col = poss.getTableColumn();

		this.data = (String) col.getCellObservableValue(item).getValue();

		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".data");
			String query = "update data set data = ? where id = 3";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, data);
			ps.execute();
			System.out.println(data + " muuttuja lähetetty tietokantaan");
			// con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Metodi asettaa oikean Stagen
	 */

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * Metodi vastaa työntekijän poistamisesta tietokannasta
	 */

	@FXML
	private void handleDelete() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText("Työntekijän poistaminen");
		alert.setContentText("Oletko varma että haluat poistaa valitun työntekijän?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			try {

				Connection con2 = AccessRegoTietokanta.getConnection();
				resultSet2 = con2.createStatement()
						.execute("DELETE FROM " + database + ".omattiedot" + " WHERE etunimi='" + data + "'");
			} catch (SQLException e) {
				e.printStackTrace();
			}

		} else {
		}
	}

	/**
	 * Metodi vastaa peruuta napin toiminnasta
	 */

	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

	/**
	 * Metodi vastaa uusi työntekijä näkymän avaamisesta
	 */

	@FXML
	private void handleUusiTyontekija() {
		showTyontekijatUusiTyöntekijaView();
	}

	/**
	 * Metodi joka vastaa työntekijat uusi työntekijä näkymän avaamisesta
	 */

	public boolean showTyontekijatUusiTyöntekijaView() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/fxml/tyontekijat_uusi_tyontekija_view.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Rego!"); // tästä otettu pois Uusi Tyontekija ja laitettu Rego! jotta ei tule ongelmia
											// käännöksien kanssa
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage); // primary stage
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			TyonkejatUusiTyöntekijaController controller = loader.getController();
			controller.setDialogStage(dialogStage);

			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Metodi joka vastaa työntekijät muokkaa työntekijää näkymän avaamisesta
	 */

	public boolean showTyontekijatMuokkaaTyöntekijaaView() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/fxml/tyontekijat_muokkaa_tyontekijaa_view.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Rego!"); // tästä otettu pois Muokkaa Tyontekijaa ja laitettu Rego! jotta ei tule
											// ongelmia käännöksien kanssa
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage); // primary stage
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			TyontekijatMuokkaaTyontekijaaController controller = loader.getController();
			controller.setDialogStage(dialogStage);

			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Metodi joka asettaa oikean Stagen
	 */

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
	 * Metodi joka tarkistaa onko Ok nappia painettu
	 */

	public boolean isOkClicked() {
		return okClicked;
	}
}
