package com.ryhma_9.rego_view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.ryhma_9.rego_util.AccessRegoTietokanta;
import com.ryhma_9.rego_util.Tyotehtava;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class OmattiedotMuokkausController {
	private ResultSet resultSet = null;
	final private String database = "rego";

	private int id;

	@FXML
	private TextField name;
	@FXML
	private TextField lastname;
	@FXML
	private TextField phone;
	@FXML
	private TextField email;
	@FXML
	private TextField adress;

	// --------------------------------- //kieli käännöstä varten tehdyt muuttujat

	@FXML
	private Label name1;
	@FXML
	private Label muokkaaomiatietoja;
	@FXML
	private Label lastname1;
	@FXML
	private Label phone1;
	@FXML
	private Label email1;
	@FXML
	private Label adress1;
	@FXML
	private Button tallenna;
	@FXML
	private Button peruuta;

	// ---------------------------------

	private Stage dialogStage;

	@FXML
	private boolean okClicked = false;

	/**
	 *  initialize() metodia kutsutaan kun näkymä käynnistyy, se asettaa oikeat arvot
	*/
	
	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		name1.setText(bundle.getString("etunimi"));
		lastname1.setText(bundle.getString("sukunimi"));
		phone1.setText(bundle.getString("puhelinnumero"));
		email1.setText(bundle.getString("sahkoposti"));
		adress1.setText(bundle.getString("osoite"));
		peruuta.setText(bundle.getString("button_peruuta"));
		tallenna.setText(bundle.getString("button_tallenna"));
		muokkaaomiatietoja.setText(bundle.getString("omientietojenmuokkaus"));
		try {
			tarkistakukaonkirjautunutid();
			Connection con = AccessRegoTietokanta.getConnection();

			String sql = "select * from " + database + ".omattiedot where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			resultSet = ps.executeQuery();

			while (resultSet.next()) {
				name.setText(resultSet.getString("etunimi"));
				lastname.setText(resultSet.getString("sukunimi"));
				phone.setText(resultSet.getString("puh"));
				email.setText(resultSet.getString("spost"));
				adress.setText(resultSet.getString("posti"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi joka tarkistaa kirjautujan id:n. Tieto haetaan tietokannasta.
	 */
	public void tarkistakukaonkirjautunutid() {

		try {
			Connection con2 = AccessRegoTietokanta.getConnection();
			String sql2 = "select * from rego.data where id=5";
			PreparedStatement ps2 = con2.prepareStatement(sql2);
			resultSet = ps2.executeQuery();

			while (resultSet.next()) {
				id = resultSet.getInt("data");
				System.out.print("OmattiedotMuokkausController tarkistaa kuka on kirjautuneena " + id);
			}
			;
			con2.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Metodi asettaa dialogstagen.
	 * @param dialogStage
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * Metodi jota kutsutaan kun Cancel painetaan, jolla suljetaan omat tiedot muokkaus ikkuna.
	 */
	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Metodi joka päivittää muokatut tiedot tietokantaan, kun käyttäjä painaa Ok ja sulkee näkymän.
	 */
	@FXML
	private void handleOk() {
		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".omattiedot");
			String query = "update omattiedot set etunimi=?, sukunimi=?, puh=?, spost=?, posti=? where id=" + id;
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, name.getText());
			ps.setString(2, lastname.getText());
			ps.setString(3, phone.getText());
			ps.setString(4, email.getText());
			ps.setString(5, adress.getText());
			ps.execute();

			okClicked = true;
			dialogStage.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}