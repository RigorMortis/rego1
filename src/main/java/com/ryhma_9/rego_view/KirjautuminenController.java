package com.ryhma_9.rego_view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import com.ryhma_9.rego.Main;
import com.ryhma_9.rego_util.AccessRegoTietokanta;

public class KirjautuminenController {
	private ResultSet resultSet = null;
	final private String database = "rego";

	@FXML
	private TextField username;
	@FXML
	private PasswordField password;

	private ResourceBundle bundle; // minni
	private Locale locale;

	@FXML
	private Button signin;
	@FXML
	private Button quit;

	@FXML
	private Label kayttajanimi;
	@FXML
	private Label sala;

	private String kayttaja;

	private String salasana;

	public String kuka = "";

	private int id;

	private Stage dialogStage;

	@FXML
	private boolean okClicked = false;

	@FXML
	private Label label;

	private Main meinApp;

	/**
	 * Konstruktori
	 */

	public KirjautuminenController() {
	}

	/**
	 * intialize() metodia kutsutaan kun näkymä käynnistetään
	 */

	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		kayttajanimi.setText(bundle.getString("username"));
		sala.setText(bundle.getString("password"));
		signin.setText(bundle.getString("signin"));
		quit.setText(bundle.getString("quit"));

		kayttaja = username.getText();
		salasana = password.getText();

		username.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) {
				password.requestFocus();
			} else if (event.getCode() == KeyCode.ENTER) {
				handleLogin();
			}
		});

		password.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) {
				handleLogin();
			}
		});
	}

	/**
	 * Metodi tallentaa tietokantaan kuka on kirjautunut ja hänen asemansa
	 * (työntekijä vai työnantaja).
	 */

	public void tallennakukaonkirjautunut() {

		try {
			Connection con1 = AccessRegoTietokanta.getConnection();
			String sql1 = "update data set data=? where id =4";
			PreparedStatement ps1 = con1.prepareStatement(sql1);
			ps1.setString(1, kuka);
			ps1.execute();
			String sql2 = "update data set data=? where id =5";
			PreparedStatement ps2 = con1.prepareStatement(sql2);
			ps2.setInt(1, id);
			ps2.execute();
			System.out.print(
					"Kuka on kirjautuneena tieto lähetetty tietokantaan KirjautuminenControllerista " + kuka + id);

			con1.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	/**
	 * Metodi luo ilmentymä Main luokasta
	 */

	public void setMeinApp(Main meinApp) {
		this.meinApp = meinApp;
	}

	/**
	 * Metodi hoitaa sisäänkirjautumisen ja käyttää tallennakukaonkirjautunut
	 * metodia.
	 */
	@FXML
	private void handleLogin() {
		kayttaja = "";
		salasana = "";
		kayttaja = username.getText();
		salasana = password.getText();
		label.setText("The name you entered is " + username.getText() + password.getText());
		try {
			Connection con = AccessRegoTietokanta.getConnection();
			String sql = "select * from rego.tunnukset where tunnus=? and salasana=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, kayttaja);
			ps.setString(2, salasana);
			resultSet = ps.executeQuery();

			if (resultSet.next()) {
				if (resultSet.getInt("boss") == 1) {
					kuka = "boss";
					id = resultSet.getInt("sid");
					con.close();
					tallennakukaonkirjautunut();
					meinApp.showTyonantajaView();

				} else {
					kuka = "worker";
					id = resultSet.getInt("sid");
					con.close();
					tallennakukaonkirjautunut();
					meinApp.showTyontekijaView();
				}
			} else {
				label.setText("Access Denied");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Metodi asettaa dialogstagen.
	 * 
	 * @param dialogStage
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * Metodia joka vastaa quitin painamisesta.
	 */
	@FXML
	private void handleQuit() {
		Platform.exit();
	}

	/**
	 * Metodi tarkistaa onko Ok nappia painettu
	*/

	public boolean isOkClicked() {
		return okClicked;
	}

	// metodi buttonnille joka vaihtaa kieltä en>en
	/**
	 * Metodi vaihtaa kielen englanniksi.
	 * 
	 * @param event
	 */
	@FXML
	private void handleBtnEN(ActionEvent event) {
		loadLang("en", "en");
	}

	/**
	 * Metodi vaihtaa kielen venäjäksi.
	 * 
	 * @param event
	 */
	@FXML
	private void handleBtnRU(ActionEvent event) {
		loadLang("ru", "ru");
	}

	/**
	 * Metodi vaihtaa kielen ruotsiksi.
	 * 
	 * @param event
	 */
	@FXML
	private void handleBtnSWE(ActionEvent event) {
		loadLang("swe", "swe");
	}

	/**
	 * Metodi vaihtaa kielen saksaksi.
	 * 
	 * @param event
	 */
	@FXML
	private void handleBtnDE(ActionEvent event) {
		loadLang("de", "de");
	}

	// metodi buttonille joka vaihtaa kieltä fi>fi
	/**
	 * Metodi vaihtaa kielen suomeksi.
	 * 
	 * @param event
	 */
	@FXML
	private void handleBtnFIN(ActionEvent event) {
		loadLang("fi", "fi");
	}

	// metodi joka vastaa kielen lataamisesta ja asettamisesta
	/**
	 * Metodi joka vastaa kielen lataamisesta ja vaihtamisesta.
	 * 
	 * @param event
	 */
	public void loadLang(String lang1, String lang2) {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(lang1, lang2);
		ResourceBundle bundle = controller.getYourBundle();
		kayttajanimi.setText(bundle.getString("username"));
		sala.setText(bundle.getString("password"));
		signin.setText(bundle.getString("signin"));
		quit.setText(bundle.getString("quit"));
	}
}