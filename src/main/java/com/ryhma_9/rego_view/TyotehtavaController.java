package com.ryhma_9.rego_view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import com.ryhma_9.rego_util.AccessRegoTietokanta;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class TyotehtavaController {

	@FXML
	private TextField taskname;
	@FXML
	private DatePicker taskdate;
	@FXML
	private TextField tasktime;
	@FXML
	private TextField cname;
	@FXML
	private TextField cphone;
	@FXML
	private TextField cadress;
	@FXML
	private TextArea otherinfo;
	
	// --------------------------------- //Kieli käännöstä varten tehdyt muuttujat
	
	@FXML
	private Label tt_nimi;
	@FXML
	private Label tt_tiedot;
	@FXML
	private Label tt_pvm;
	@FXML
	private Label tt_clo;
	@FXML
	private Label asiakas_nimi;
	@FXML
	private Label asiakas_puh;
	@FXML
	private Label asiakas_osoite;
	@FXML
	private Label tarkentavaa_tietoa;
	@FXML
	private Button peruuta;
	@FXML
	private Button tallenna;

	// ---------------------------------
	
	private ResultSet resultSet = null;
	final private String database = "rego";
	private String errorMessage = "";
	private Stage dialogStage;
	private boolean okClicked = false;
	
	/**
	 * intialize() metodia kutsutaan kun näkymä käynnistyy, metodi ottaa yhteyttä tietokantaan ja asettaa oikeat arvot kenttiin
	*/
	
	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		tt_nimi.setText(bundle.getString("tyotehtavannimi"));
		tt_tiedot.setText(bundle.getString("tyotehtavantiedot"));
		tt_pvm.setText(bundle.getString("pvm"));
		tt_clo.setText(bundle.getString("kellonaika"));
		asiakas_nimi.setText(bundle.getString("asiakkaannimi"));
		asiakas_puh.setText(bundle.getString("asiakkaanpuhelinnumero"));
		asiakas_osoite.setText(bundle.getString("asiakkaanosoite"));
		tarkentavaa_tietoa.setText(bundle.getString("tarkentavaatietoa"));
		peruuta.setText(bundle.getString("button_peruuta"));
		tallenna.setText(bundle.getString("button_tallenna"));
	}

	/**
	 * Metodi asettaa oikean Stagen
	*/
	
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * Metodi tarkistaa onko Ok nappia painettu
	*/
	
	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 *  Metodi asettaa tietokanta taulukkoon valitut tiedot jos Ok nappia on painettu
	*/
	
	@FXML
	private void handleOk() {
		if (isInputValid()) {
			try {
				Connection con = AccessRegoTietokanta.getConnection();
				resultSet = con.createStatement().executeQuery("select * from " + database + ".tyotehtavat");
				String query = "insert into tyotehtavat(ttnimi,klo,pvm,animi,apuh,aosoite,ttieto) values(?,?,?,?,?,?,?)";
				PreparedStatement ps = con.prepareStatement(query);
				ps.setString(1, taskname.getText());
				ps.setString(2, tasktime.getText());
				ps.setString(3, ((TextField) taskdate.getEditor()).getText());
				ps.setString(4, cname.getText());
				ps.setString(5, cphone.getText());
				ps.setString(6, cadress.getText());
				ps.setString(7, otherinfo.getText());
				ps.execute();

				okClicked = true;
				dialogStage.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Metodi vastaa peruuta näppäimen toiminnasta
	*/
	
	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

	/**
	 * Metodi joka vastaa käyttäjän syötteiden tarkistuksesta
	*/
	
	private boolean isInputValid() {

		if (taskname.getText() == null || taskname.getText().length() == 0) {
			errorMessage += "- Työtehtävän nimi virheellinen.\n";
		}
		if (tasktime.getText() == null || tasktime.getText().length() == 0) {
			errorMessage += "- Ajankohta virheellinen.\n";
		}
		if (cname.getText() == null || cname.getText().length() == 0) {
			errorMessage += "- Asiakkaan nimi puuttuu.\n";
		}
		if (cphone.getText() == null || cphone.getText().length() == 0) {
			errorMessage += "- Asiakkaan puhelinnumero virheellinen.\n";
		} else {
			try {
				Integer.parseInt(cphone.getText());
			} catch (NumberFormatException e) {
				errorMessage += "- Virheellinen puhelinnumero.\n";
			}
		}
		if (cadress.getText() == null || cadress.getText().length() == 0) {
			errorMessage += "- Asiakkaan osoite puuttuu.\n";
		}
		if (errorMessage.length() == 0) {
			return true;
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Virheilmoitus");
			alert.setHeaderText("Korjaa virheelliset kentät");
			alert.setContentText(errorMessage);
			alert.showAndWait();

			return false;
		}
	}
}