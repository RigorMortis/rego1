package com.ryhma_9.rego_view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import com.ryhma_9.rego.Main;
import com.ryhma_9.rego_util.AccessRegoTietokanta;
import com.ryhma_9.rego_util.Tyotehtava;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

public class TyontekijaController {
	private ResultSet resultSet = null;
	final private String database = "rego";

	@FXML
	private TableView<Tyotehtava> table_tt;
	@FXML
	private ObservableList<Tyotehtava> tyotehtavat = FXCollections.observableArrayList();
	@FXML
	private TableColumn<Tyotehtava, String> col_työteht;
	@FXML
	private TableColumn<Tyotehtava, String> col_pvm;
	@FXML
	private Main mainApp;
	public String data;
	@FXML
	private Button tiedot;
	@FXML
	private Button kirjauduulos;
	@FXML
	private Button yhteystiedot;
	@FXML
	private Button button_refresh;
	@FXML
	private Button oma_tt;

	// ---------- Kieliä varten
	@FXML
	private Button button_tt;

	// ----------

	/**
	 * Konstruktori jota kutsutaan ennen initialize() metodia
	 */

	public TyontekijaController() {
	}

	/**
	 * Metodi joka luo ilmentymän Main luokasta
	 */

	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
	}

	/**
	 * initialize() metodia kutsutaan kun näkymä käynnistyy, metodi ottaa yhteyttä
	 * tietokantaan ja asettaa arvot taulukkoon
	 */

	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		tiedot.setText(bundle.getString("button_omat"));
		kirjauduulos.setText(bundle.getString("button_out"));
		yhteystiedot.setText(bundle.getString("yhteystiedot"));
		col_työteht.setText(bundle.getString("col_tyoteht"));
		col_pvm.setText(bundle.getString("col_pvm"));
		button_refresh.setText(bundle.getString("button_refresh"));
		button_tt.setText(bundle.getString("button_omatyotehtava"));

		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".tyotehtavat");
			while (resultSet.next()) {
				tyotehtavat.add(new Tyotehtava(resultSet.getString("ttnimi"), resultSet.getString("pvm")));
				col_työteht.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("ttnimi"));
				col_pvm.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("pvm"));
				// tyotehtavat.add(jaffa.getTtnimi());
				table_tt.setItems(tyotehtavat);
				table_tt.setOnMousePressed(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {
						if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {

							valituntehtavanid();
							handleTyotehtavaTarkastelu();
						} else if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {

							valituntehtavanid();
						}
					}

				});

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi tallentaan tietokantaan valitun työtehtävän id arvon, jota myöhemmin
	 * käytetään avaamaan oikean työtehtävän lisätiedot.
	 */

	@FXML
	void valituntehtavanid() {
		int pos = table_tt.getSelectionModel().getSelectedIndex();
		TablePosition poss = table_tt.getSelectionModel().getSelectedCells().get(0);
		int row = poss.getRow();
		Tyotehtava item = table_tt.getItems().get(row);
		TableColumn col = poss.getTableColumn();

		this.data = (String) col.getCellObservableValue(item).getValue();

		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".data");
			String query = "update data set data = ? where id = 1";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, data);
			ps.execute();
			System.out.println(data + " muuttuja lähetetty tietokantaan");
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		TyotehtavaTarkasteluController tyotehtavaTarkasteluController = new TyotehtavaTarkasteluController();
	

		

		System.out.println("Olen TyonantajaControllerin printti, data muuttuja sai arvon " + data);
		System.out.println(
				"Olen TyonantajaControllerin printti, kutsun testin vuoksi public String getData() metodia, se antaa "
						+ getData());

	}

	/**
	 * Metodi hoitaa taulukon päivittämisen
	 */

	@FXML
	private void handleRefresh() {
		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".tyotehtavat");
			table_tt.getItems().clear();
			while (resultSet.next()) {
				tyotehtavat.add(new Tyotehtava(resultSet.getString("ttnimi"), resultSet.getString("pvm")));
				col_työteht.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("ttnimi"));
				col_pvm.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("pvm"));
				// tyotehtavat.add(jaffa.getTtnimi());
				table_tt.setItems(tyotehtavat);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Hakee data muuttujan
	 */

	public String getData() {
		return data;
	}

	/**
	 * Metodi vastaa työtehtävä tarkastelu näkymän avaamisesta
	 */

	@FXML
	private void handleTyotehtavaTarkastelu() {

		mainApp.showTyotehtavaTarkasteluView();
	}

	/**
	 * Metodi vastaa kirjautumis näkymän avaamisesta
	 */

	@FXML
	private void handleKirjautuminen() {
		mainApp.showKirjautuminenView();
	}

	/**
	 * Metodi vastaa omat tiedot näkymän avaamisesta
	 */

	@FXML
	private void handleuusityotehtava() {
		mainApp.showTyotehtavaView();
	}

	/**
	 * Metodi vastaa omat tiedot näkymän avaamisesta
	 */

	@FXML
	private void handleOmattiedot() {
		mainApp.showOmattiedotView();
	}

	/**
	 * Metodi vastaa työntekijät näkymän avaamisesta
	 */

	@FXML
	private void handleTyontekijat() {
		mainApp.showTyontekijatView();
	}

	/**
	 * Metodi vastaa oma työtehtävä näkymän avaamisesta
	 */

	@FXML
	private void handleOmaTyotehtava() {
		mainApp.showOmaTyotehtavaView();
	}

	/**
	 * Metodi vastaa englannin kieleen vaihdosta
	 */

	@FXML
	private void handleBtnEN(ActionEvent event) {
		loadLang("en", "en");
	}

	/**
	 * Metodi vastaa venäjän kieleen vaihdosta
	 */

	@FXML
	private void handleBtnRU(ActionEvent event) {
		loadLang("ru", "ru");
	}

	/**
	 * Metodi vastaa ruotsin kieleen vaihdosta
	 */

	@FXML
	private void handleBtnSWE(ActionEvent event) {
		loadLang("swe", "swe");
	}

	/**
	 * Metodi vastaa saksan kieleen vaihdosta
	 */

	@FXML
	private void handleBtnDE(ActionEvent event) {
		loadLang("de", "de");
	}

	/**
	 * Metodi vastaa suomen kieleen vaihdosta
	 */

	@FXML
	private void handleBtnFIN(ActionEvent event) {
		loadLang("fi", "fi");
	}

	/**
	 * Metodi vastaa valitun kielen asetuksesta
	 */

	private void loadLang(String lang1, String lang2) {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(lang1, lang2);
		ResourceBundle bundle = controller.getYourBundle();
		tiedot.setText(bundle.getString("button_omat"));
		kirjauduulos.setText(bundle.getString("button_out"));
		yhteystiedot.setText(bundle.getString("yhteystiedot"));
		col_työteht.setText(bundle.getString("col_tyoteht"));
		col_pvm.setText(bundle.getString("col_pvm"));
		button_refresh.setText(bundle.getString("button_refresh"));
		button_tt.setText(bundle.getString("button_omatyotehtava"));

	}
}
