package com.ryhma_9.rego_view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.ryhma_9.rego_util.AccessRegoTietokanta;
import com.ryhma_9.rego_util.Tyotehtava;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

public class TyotehtavaTarkasteluController {
	
	
	private ResultSet resultSet = null;
	final private String database = "rego";
	Connection con;

	@FXML
	private TableView<Tyotehtava> table_tt;

	@FXML
	private ObservableList<Tyotehtava> tyotehtavat = FXCollections.observableArrayList();

	private int id;
	private String superdata;
	private String tasknames;
	private String taskdates;
	private String tasktimes;
	private String cnames;
	private String cphones;
	private String cadresss;
	private String otherinfos;
	
	@FXML
	private Label taskname;
	@FXML
	private Label taskdate;
	@FXML
	private Label tasktime;
	@FXML
	private Label cname;
	@FXML
	private Label cphone;
	@FXML
	private Label cadress;
	@FXML
	private Label otherinfo;
	
	// ------------------------------ //Kieli käännöstä varten tehdyt muuttujat
	@FXML
	private Label taskinfo;
	@FXML
	private Label taskname1;
	@FXML
	private Label taskdate1;
	@FXML
	private Label tasktime1;
	@FXML
	private Label cname1;
	@FXML
	private Label cphone1;
	@FXML
	private Label cadress1;
	@FXML
	private Label otherinfo1;
	@FXML
	private Button hyvaksy;
	@FXML
	private Button peruuta;

	private Stage dialogStage;

	private boolean okClicked = false;

	TyonantajaController tyonantajacontroller = new TyonantajaController();
	
	/**
	 * initialize() metodia kutsutaan kun käynnistetään näkymä, metodi asettaa oikeat arvot kenttiin
	*/
	
	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		taskname1.setText(bundle.getString("tyotehtavannimi"));
		taskinfo.setText(bundle.getString("tyotehtavantiedot"));
		taskdate1.setText(bundle.getString("pvm"));
		tasktime1.setText(bundle.getString("kellonaika"));
		cname1.setText(bundle.getString("asiakkaannimi"));
		cphone1.setText(bundle.getString("asiakkaanpuhelinnumero"));
		cadress1.setText(bundle.getString("asiakkaanosoite"));
		otherinfo1.setText(bundle.getString("tarkentavaatietoa"));
		hyvaksy.setText(bundle.getString("button_hyvaksy"));
		peruuta.setText(bundle.getString("button_peruuta"));
		try {
			con = AccessRegoTietokanta.getConnection();
			String sql1 = "select * from rego.data where id=1";
			PreparedStatement ps1 = con.prepareStatement(sql1);
			resultSet = ps1.executeQuery();
			while (resultSet.next()) {
			superdata = resultSet.getString("data");
			System.out.print("HOSIANNA" + superdata);
			};
			
			String sql = "select * from rego.tyotehtavat where ttnimi=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, superdata);
			resultSet = ps.executeQuery();
			
			if(resultSet.next()) {
				tasknames = resultSet.getString("ttnimi");
				taskdates = resultSet.getString("pvm");
				tasktimes = resultSet.getString("klo");
				cnames = resultSet.getString("animi");
				cphones = resultSet.getString("apuh");
				cadresss = resultSet.getString("aosoite");
				otherinfos = resultSet.getString("ttieto");
				taskname.setText(tasknames);
				taskdate.setText(taskdates);
				tasktime.setText(tasktimes);
				cname.setText(cnames);
				cphone.setText(cphones);
				cadress.setText(cadresss);
				otherinfo.setText(otherinfos);
				
				//hyvä linkki, tosin en ite saa siitä selkoa, https://stackoverflow.com/questions/14187963/passing-parameters-javafx-fxml
				//datta = tyonantajacontroller.getData();

				con.close();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi asettaa oikean Stagen
	*/
	
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Metodi vastaa peruuta näppäimen toiminnasta
	*/
	
	@FXML
	private void handleCancel() {
		dialogStage.close();
	}
	
	/**
	 * Metodi lähettää tietokantaan syötetyt tiedot
	*/
	
	@FXML
	public void handleOk() {
		try {
			con = AccessRegoTietokanta.getConnection();			
			String sql2 = "select * from rego.data where id=5";
			PreparedStatement ps2 = con.prepareStatement(sql2);
			//ps.setString(1, superdata);
			resultSet = ps2.executeQuery();
			while (resultSet.next()) {
				id = resultSet.getInt("data");
				System.out.print("/nHOSIANNA" + id);
			};
			
			String sql = "insert into rego.omatyotehtava (ttid,ttnimi,klo,pvm,animi,apuh,aosoite,ttieto) values(?,?,?,?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.setString(2, tasknames);
			ps.setString(3, tasktimes);
			ps.setString(4, taskdates);
			ps.setString(5, cnames);
			ps.setString(6, cphones);
			ps.setString(7, cadresss);
			ps.setString(8, otherinfos);
			ps.execute();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dialogStage.close();
	}	
}
