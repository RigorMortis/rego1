package com.ryhma_9.rego_view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import com.ryhma_9.rego_util.AccessRegoTietokanta;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class TyonkejatUusiTyöntekijaController {
	private Stage primaryStage;
	private ResultSet resultSet = null;
	private ResultSet resultSet1 = null;
	final private String database = "rego";
	private Stage dialogStage;
	@FXML
	private boolean okClicked = false;
	@FXML
	private TextField etunimi;
	@FXML
	private TextField sukunimi;
	@FXML
	private TextField puh;
	@FXML
	private TextField spost;
	@FXML
	private TextField posti;

	// ------------------------ //Kieli käännöstä varten tehdyt muttujat

	@FXML
	private Label etunimi1;
	@FXML
	private Label sukunimi1;
	@FXML
	private Label puhelinnumero;
	@FXML
	private Label sahkoposti;
	@FXML
	private Label postiosoite;
	@FXML
	private Label tyontekijantiedot;
	@FXML
	private Button peruuta;
	@FXML
	private Button tallenna;

	/**
	 * initialize() metodia kutsutaan kun näkymä käynnistyy, metodi asettaa arvot
	 * kenttiin
	 */

	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		etunimi1.setText(bundle.getString("etunimi"));
		sukunimi1.setText(bundle.getString("sukunimi"));
		puhelinnumero.setText(bundle.getString("puhelinnumero"));
		sahkoposti.setText(bundle.getString("sahkoposti"));
		postiosoite.setText(bundle.getString("osoite"));
		peruuta.setText(bundle.getString("button_peruuta"));
		tallenna.setText(bundle.getString("button_tallenna"));

		tyontekijantiedot.setText(bundle.getString("tyontekijantiedot"));

	}

	/**
	 * Metodi asettaa oikean Stagen
	 */

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * Metodi tarkistaa onko OK nappia painettu
	 */

	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Metodi vastaa peruuta napin toiminnasta, sulkee näkymän
	 */

	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

	/**
	 * Metodi vastaa tietojen tallennuksesta tietokantaan
	 */

	@FXML
	private void handleTallenna() {
		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".omattiedot");
			String query = "insert into omattiedot(etunimi,sukunimi,puh,spost,posti) values(?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, etunimi.getText());
			ps.setString(2, sukunimi.getText());
			ps.setString(3, puh.getText());
			ps.setString(4, spost.getText());
			ps.setString(5, posti.getText());
			ps.execute();

			resultSet1 = con.createStatement().executeQuery("select * from " + database + ".tunnukset");
			String query1 = "insert into tunnukset(tunnus,salasana) values(?,?)";
			PreparedStatement ps1 = con.prepareStatement(query1);

			String Etunimi = etunimi.getText();
			String Sukunimi = sukunimi.getText();
			String Etunimi1 = Etunimi.substring(0, 2);
			String Sukunimi1 = Sukunimi.substring(0, 2);
			String tunnus = Etunimi1 + Sukunimi1;
			String salasana = tunnus + "1234";
			System.out.println("tunnus on " + tunnus + "salasana on " + salasana);

			ps1.setString(1, tunnus);
			ps1.setString(2, salasana);
			ps1.execute();
			con.close();

			okClicked = true;
			// luotunnukset();
			dialogStage.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
