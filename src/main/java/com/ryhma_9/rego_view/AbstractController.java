package com.ryhma_9.rego_view;

import java.util.Locale;
import java.util.ResourceBundle;


public class AbstractController implements Controller {
	private Locale locale;
	private ResourceBundle bundle;
	private static AbstractController single_instance = null;
	public String lang1;
	public String lang2;
	
	/**
	 * Konstruktori
	 */
	
	private AbstractController() { //singleton
		lang1 = "fi";
		lang2 = "fi";
	}
	
	/**
	 * Abstrakti kontrolleri
	*/
	
	public static AbstractController getInstance() {
		if (single_instance == null) 
            single_instance = new AbstractController(); 
  
        return single_instance; 
	}
	
	/**
	 * Metodi joka vastaa kielen latauksesta
	*/
	
	public void loadLanguage(String lang1, String lang2) {
        locale = new Locale(lang1);
        bundle = ResourceBundle.getBundle("com/ryhma_9/rego_languages/ApplicationResources_" + lang2, locale);
        this.lang1 = lang1;
        this.lang2 = lang2;
    }
	
	/**
	 * Metodi joka hakee ResourseBundlen kieliä varten
	*/
	
	public ResourceBundle getYourBundle() {
		return bundle;
	}

	/**
	 * Metodi joka hakee ensimmäisen kielen
	*/
	
	@Override
	public String getLang1() {
		return lang1;
	}
	
	/**
	 * Metodi joka hakee toisen kielen
	*/
	
	@Override
	public String getLang2() {
		return lang2;
	}

}
