package com.ryhma_9.rego_view;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import com.ryhma_9.rego.Main;

import com.ryhma_9.rego_util.AccessRegoTietokanta;
import com.ryhma_9.rego_util.Tyotehtava;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class TyonantajaController {
	private Connection connect = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	final private String database = "rego";

	@FXML
	private TableView<Tyotehtava> table_tt;

	@FXML
	private ObservableList<Tyotehtava> tyotehtavat = FXCollections.observableArrayList();

	@FXML
	private TableColumn<Tyotehtava, String> col_tyoteht;

	@FXML
	private TableColumn<Tyotehtava, String> col_pvm;

	@FXML
	private Label firstNameLabel;
	@FXML
	private Label lastNameLabel;
	@FXML
	private Label streetLabel;
	@FXML
	private Label postalCodeLabel;
	@FXML
	private Label cityLabel;
	@FXML
	private Label birthdayLabel;
	private int id;
	public String data;

	// Reference to the main application.
	private Main mainApp;

	// --------------------------------//kieli käännöstä varten tehdyt muuttujat

	@FXML
	private Button button_omat;
	@FXML
	private Button button_tt;
	@FXML
	private Button button_out;
	@FXML
	private Button button_delete;
	@FXML
	private Button button_refresh;
	@FXML
	private Button button_lisaa;

	// --------------------------------

	/**
	 * Konstruktori jota kutsutaan ennen initialize() metodia
	 */

	public TyonantajaController() {
	}

	/**
	 * Luodaan ilmentymä Main luokasta
	 */

	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
	}

	/**
	 * initialize() metodia kutsutaan kun näkymä käynnistyy, metodi ottaa yhteyttä
	 * tietokantaan ja asettaa arvot taulukkoon
	 */

	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		button_omat.setText(bundle.getString("button_omat"));
		button_out.setText(bundle.getString("button_out"));
		button_tt.setText(bundle.getString("button_tt"));
		col_tyoteht.setText(bundle.getString("col_tyoteht"));
		col_pvm.setText(bundle.getString("col_pvm"));
		button_delete.setText(bundle.getString("button_delete"));
		button_lisaa.setText(bundle.getString("button_lisaatyotehtava"));
		button_refresh.setText(bundle.getString("button_refresh"));

		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".tyotehtavat");
			while (resultSet.next()) {
				tyotehtavat.add(new Tyotehtava(resultSet.getString("ttnimi"), resultSet.getString("pvm")));
				col_tyoteht.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("ttnimi"));
				col_pvm.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("pvm"));
				table_tt.setItems(tyotehtavat);
				table_tt.setOnMousePressed(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {
						if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {

							valituntehtavanid();
							handleTyotehtavaTarkastelu();
						} else if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {

							valituntehtavanid();
						}
					}

				});

			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi hoitaa työtehtävän poistamisen taulukosta
	 */

	@FXML
	private void handlePoistaTyöTehtävä() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText("Työtehtävän poistaminen");
		alert.setContentText("Haluatko varmasti poistaa työtehtävän?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) { // ... user chose OK
			Connection connect = null;
			Statement statement = null;
			boolean resultSet1;
			Connection con;
			try {
				con = AccessRegoTietokanta.getConnection();
				resultSet1 = con.createStatement()

						.execute("DELETE FROM " + database + ".tyotehtavat" + " WHERE ttnimi='" + data + "'");
				System.out.print("DELETE FROM " + database + ".tyotehtavat" + " WHERE ttnimi='" + data + "'");
				con.close();

				con = AccessRegoTietokanta.getConnection();
				resultSet = con.createStatement().executeQuery("select * from " + database + ".tyotehtavat");
				table_tt.getItems().clear();
				while (resultSet.next()) {
					tyotehtavat.add(new Tyotehtava(resultSet.getString("ttnimi"), resultSet.getString("pvm")));

					col_tyoteht.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("ttnimi"));
					col_pvm.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("pvm"));
					// tyotehtavat.add(jaffa.getTtnimi());
					table_tt.setItems(tyotehtavat);

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		} else {
			// ... user chose CANCEL or closed the dialog
		}
	}

	/**
	 * Metodi hoitaa taulukon päivittämisen
	 */

	@FXML
	private void handleRefresh() {
		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".tyotehtavat");
			table_tt.getItems().clear();
			while (resultSet.next()) {
				tyotehtavat.add(new Tyotehtava(resultSet.getString("ttnimi"), resultSet.getString("pvm")));

				col_tyoteht.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("ttnimi"));
				col_pvm.setCellValueFactory(new PropertyValueFactory<Tyotehtava, String>("pvm"));
				table_tt.setItems(tyotehtavat);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi tallentaan tietokantaan valitun työtehtävän id arvon, jota myöhemmin
	 * käytetään avaamaan oikean työtehtävän lisätiedot.
	 */

	@FXML
	void valituntehtavanid() {
		int pos = table_tt.getSelectionModel().getSelectedIndex();
		TablePosition poss = table_tt.getSelectionModel().getSelectedCells().get(0);
		int row = poss.getRow();
		Tyotehtava item = table_tt.getItems().get(row);
		TableColumn col = poss.getTableColumn();

		this.data = (String) col.getCellObservableValue(item).getValue();

		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".data");
			String query = "update data set data = ? where id = 1";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, data);
			ps.execute();
			System.out.println(data + " muuttuja lähetetty tietokantaan");
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		TyotehtavaTarkasteluController tyotehtavaTarkasteluController = new TyotehtavaTarkasteluController();
		
		System.out.println("Olen TyonantajaControllerin printti, data muuttuja sai arvon " + data);
		System.out.println(
				"Olen TyonantajaControllerin printti, kutsun testin vuoksi public String getData() metodia, se antaa "
						+ getData());
		// System.out.println("INDEX:" + pos + "\nTunnus:" + item + "\nTyotehtava:" +
		// data);

	}

	/**
	 * Hakee data muuttujan
	 */

	public String getData() {
		return data;
	}

	/**
	 * Metodi vastaa kirjautumis näkymän avaamisesta
	 */

	@FXML
	private void handleKirjautuminen() {
		mainApp.showKirjautuminenView();
	}

	/**
	 * Metodi vastaa uusi työtehtävä näkymän avaamisesta
	 */

	@FXML
	private void handleuusityotehtava() {
		mainApp.showTyotehtavaView();
	}

	/**
	 * Metodi vastaa omat tiedot näkymän avaamisesta
	 */

	@FXML
	private void handleOmattiedot() {
		mainApp.showOmattiedotView();
	}

	/**
	 * Metodi vastaa työtehtävä tarkastelu näkymän avaamisesta
	 */

	@FXML
	private void handleTyotehtavaTarkastelu() {

		mainApp.showTyotehtavaTarkasteluView();
	}

	/**
	 * Metodi vastaa työntekijät näkymän avaamisesta
	 */

	@FXML
	private void handleTyontekijat() {
		mainApp.showTyontekijatView();
	}

	/**
	 * Metodi vastaa englannin kieleen vaihdosta
	 */

	@FXML
	private void handleBtnEN(ActionEvent event) {
		loadLang("en", "en");
	}

	/**
	 * Metodi vastaa venäjän kieleen vaihdosta
	 */

	@FXML
	private void handleBtnRU(ActionEvent event) {
		loadLang("ru", "ru");
	}

	/**
	 * Metodi vastaa ruotsin kieleen vaihdosta
	 */

	@FXML
	private void handleBtnSWE(ActionEvent event) {
		loadLang("swe", "swe");
	}

	/**
	 * Metodi vastaa saksan kieleen vaihdosta
	 */

	@FXML
	private void handleBtnDE(ActionEvent event) {
		loadLang("de", "de");
	}

	/**
	 * Metodi vastaa suomen kieleen vaihdosta
	 */

	@FXML
	private void handleBtnFIN(ActionEvent event) {
		loadLang("fi", "fi");
	}

	/**
	 * Metodi vastaa valitun kielen asetuksesta
	 */

	private void loadLang(String lang1, String lang2) {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(lang1, lang2);
		ResourceBundle bundle = controller.getYourBundle();
		button_omat.setText(bundle.getString("button_omat"));
		button_out.setText(bundle.getString("button_out"));
		button_tt.setText(bundle.getString("button_tt"));
		col_tyoteht.setText(bundle.getString("col_tyoteht"));
		col_pvm.setText(bundle.getString("col_pvm"));
		button_delete.setText(bundle.getString("button_delete"));
		button_lisaa.setText(bundle.getString("button_lisaatyotehtava"));
		button_refresh.setText(bundle.getString("button_refresh"));
	}
}