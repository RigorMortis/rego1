package com.ryhma_9.rego_view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import com.ryhma_9.rego_util.AccessRegoTietokanta;
import com.ryhma_9.rego_util.Tyotehtava;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class OmatTyotehtavatController {
	private ResultSet resultSet = null;
	final private String database = "rego";

	@FXML
	private TableView<Tyotehtava> table_tt;

	@FXML
	private ObservableList<Tyotehtava> tyotehtavat = FXCollections.observableArrayList();

	private int superdata;
	@FXML
	private Label taskname;
	@FXML
	private Label taskdate;
	@FXML
	private Label tasktime;
	@FXML
	private Label cname;
	@FXML
	private Label cphone;
	@FXML
	private Label cadress;
	@FXML
	private Label otherinfo;

	// ------------- Kieliä varten
	@FXML
	private Label tt_nimii;
	@FXML
	private Label tt_tiedott;
	@FXML
	private Label tt_pvm;
	@FXML
	private Label tt_clo;
	@FXML
	private Label asiakas_nimi;
	@FXML
	private Label asiakas_puh;
	@FXML
	private Label asiakas_osoite;
	@FXML
	private Label tarkentavaa_tietoa;
	@FXML
	private Label tyotehtavasitiedot;
	@FXML
	private Button button_suoritettu;
	@FXML
	private Button button_peruuta;

	// -------------

	private Stage dialogStage;

	private boolean okClicked = false;

	TyonantajaController tyonantajacontroller = new TyonantajaController();

	private int id;

	/**
	 *  initialize() metodia kutsutaan kun näkymä käynnistyy, se asettaa oikeat arvot
	*/
	
	@FXML
	private void initialize() {
		try {

			AbstractController controller = AbstractController.getInstance();
			controller.loadLanguage(controller.getLang1(), controller.getLang2());
			ResourceBundle bundle = controller.getYourBundle();
			button_suoritettu.setText(bundle.getString("button_suoritettu"));

			tyotehtavasitiedot.setText(bundle.getString("tyotehtavasitiedot"));

			button_peruuta.setText(bundle.getString("button_peruuta"));

			tt_nimii.setText(bundle.getString("tyotehtavannimi"));
			
			tt_pvm.setText(bundle.getString("pvm"));
			tt_clo.setText(bundle.getString("kellonaika"));
			asiakas_nimi.setText(bundle.getString("asiakkaannimi"));
			asiakas_puh.setText(bundle.getString("asiakkaanpuhelinnumero"));
			asiakas_osoite.setText(bundle.getString("asiakkaanosoite"));
			tarkentavaa_tietoa.setText(bundle.getString("tarkentavaatietoa"));

			tarkistakukaonkirjautunutid();
			Connection con = AccessRegoTietokanta.getConnection();
			String sql = "select * from " + database + ".omatyotehtava where ttid=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			resultSet = ps.executeQuery();

			while (resultSet.next()) {
				taskname.setText(resultSet.getString("ttnimi"));
				taskdate.setText(resultSet.getString("pvm"));
				tasktime.setText(resultSet.getString("klo"));
				cname.setText(resultSet.getString("animi"));
				cphone.setText(resultSet.getString("apuh"));
				cadress.setText(resultSet.getString("aosoite"));
				otherinfo.setText(resultSet.getString("ttieto"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi asettaa dialogstagen.
	 * @param dialogStage
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Metodi jota kutsutaan kun Cancel painetaan, jolla suljetaan omat tiedot muokkaus ikkuna.
	 */
	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

	/**
	 * Metodi joka kutsutaan, kun työntekijä on suotittanut työtehtävänsä. Metodi tyhjentää ja sulkee omat työtehtävät näkymän.
	 */
	@FXML
	private void handleSuoritettu() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText("Työtehtävän poistaminen");
		alert.setContentText("Haluatko varmasti poistaa työtehtävän?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) { // ... user chose OK

			Connection con1;
			try {
				con1 = AccessRegoTietokanta.getConnection();

				boolean resultSet1 = con1.createStatement()
						.execute("DELETE FROM " + database + ".omatyotehtava" + " WHERE ttid='" + id + "'");

				dialogStage.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			// ... user chose CANCEL or closed the dialog
		}
	}

	/**
	 * Metodi joka tarkistaa kirjautujan id:n. Tieto haetaan tietokannasta.
	 */
	public void tarkistakukaonkirjautunutid() {

		try {
			Connection con2 = AccessRegoTietokanta.getConnection();
			String sql2 = "select * from rego.data where id=5";
			PreparedStatement ps2 = con2.prepareStatement(sql2);
			resultSet = ps2.executeQuery();

			while (resultSet.next()) {
				id = resultSet.getInt("data");
				System.out.print("OmattiedotController tarkistaa kuka on kirjautuneena " + id);
			}
			;
			con2.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
