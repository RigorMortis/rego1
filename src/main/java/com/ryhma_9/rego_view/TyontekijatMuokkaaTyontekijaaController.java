package com.ryhma_9.rego_view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import com.ryhma_9.rego_util.AccessRegoTietokanta;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class TyontekijatMuokkaaTyontekijaaController {
	private Stage primaryStage;
	private ResultSet resultSet = null;
	final private String database = "rego";
	private Stage dialogStage;
	@FXML
	private boolean okClicked = false;
	@FXML
	private TextField etunimi;
	@FXML
	private TextField sukunimi;
	@FXML
	private TextField puh;
	@FXML
	private TextField spost;
	@FXML
	private TextField posti;
	
	// ------------------------------ //Kieli käännöstä varten tehdyt muuttujat
	@FXML
	private Label etunimi1;
	@FXML
	private Label sukunimi1;
	@FXML
	private Label puh1;
	@FXML
	private Label spost1;
	@FXML
	private Label posti1;
	@FXML
	private Label tyontekijatiedot;
	@FXML
	private Button peruuta;
	@FXML
	private Button tallenna;
	
	

	String superdata;
	private int id;

	/**
	 * initialize() metodia kutsutaan kun näkymä käynnistyy, metodi asettaa arvot
	 */

	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		etunimi1.setText(bundle.getString("etunimi"));
		sukunimi1.setText(bundle.getString("sukunimi"));
		puh1.setText(bundle.getString("puhelinnumero"));
		spost1.setText(bundle.getString("sahkoposti"));
		posti1.setText(bundle.getString("osoite"));
		tyontekijatiedot.setText(bundle.getString("tyontekijantiedot"));
		tallenna.setText(bundle.getString("button_tallenna"));
		peruuta.setText(bundle.getString("button_peruuta"));
		try {
			Connection con = AccessRegoTietokanta.getConnection();
			String sql = "select * from rego.data where id=3";
			PreparedStatement ps = con.prepareStatement(sql);
			resultSet = ps.executeQuery();
			

			while (resultSet.next()) {
				superdata = resultSet.getString("data");
				System.out.print("HOSIANNA2" + superdata);
			}
			;

			String sql1 = "select * from rego.omattiedot where etunimi=?";
			PreparedStatement ps1 = con.prepareStatement(sql1);
			ps1.setString(1, superdata);
			resultSet = ps1.executeQuery();
			if (resultSet.next()) {
				id = resultSet.getInt("id");
				etunimi.setText(resultSet.getString("etunimi"));
				sukunimi.setText(resultSet.getString("sukunimi"));
				puh.setText(resultSet.getString("puh"));
				spost.setText(resultSet.getString("spost"));
				posti.setText(resultSet.getString("posti"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi asettaa oikean Stagen
	 */

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * Metodi vastaa peruuta näppäimen toiminnasta
	 */

	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

	/**
	 * Metodi tarkistaa onko ok nappia painettu
	 */

	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Metodi tallentaa tietokantaan uudet omattiedot
	 */

	@FXML
	private void handleTallenna() {
		try {
			Connection con = AccessRegoTietokanta.getConnection();
			resultSet = con.createStatement().executeQuery("select * from " + database + ".omattiedot");
			String query = "update omattiedot set etunimi=?, sukunimi=?, puh=?, spost=?, posti=? where id=" + id;
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, etunimi.getText());
			ps.setString(2, sukunimi.getText());
			ps.setString(3, puh.getText());
			ps.setString(4, spost.getText());
			ps.setString(5, posti.getText());
			ps.execute();

			okClicked = true;
			dialogStage.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
