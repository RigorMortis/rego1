package com.ryhma_9.rego_view;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.ryhma_9.rego.Main;
import com.ryhma_9.rego_util.AccessRegoTietokanta;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class OmattiedotController {
	private ResultSet resultSet = null;
	final private String database = "rego";
	private Stage primaryStage;

	// --------------------------------- //nää on tietokanta hakua varten

	@FXML
	private Label name;
	@FXML
	private Label lastname;
	@FXML
	private Label phone;
	@FXML
	private Label email;
	@FXML
	private Label adress;

	// --------------------------------- //kieli käännöstä varten tehdyt muuttujat

	@FXML
	private Label name1;
	@FXML
	private Label lastname1;
	@FXML
	private Label phone1;
	@FXML
	private Label email1;
	@FXML
	private Label adress1;

	// ---------------------------------

	@FXML
	private Button peruuta;
	@FXML
	private Button paivita;
	@FXML
	private Button muokkaa;
	@FXML
	private Label omattiedot;

	private Stage dialogStage;
	@FXML
	private boolean okClicked = false;
	private int id;

	String kuka = "";

	/**
	 * Konstruktori
	*/
	
	public OmattiedotController() {
	}

	/**
	 * initialize() metodia kutsutaan näkymän käynnistyessä, se asettaa oikeat arvot
	*/
	
	@FXML
	private void initialize() {
		AbstractController controller = AbstractController.getInstance();
		controller.loadLanguage(controller.getLang1(), controller.getLang2());
		ResourceBundle bundle = controller.getYourBundle();
		name1.setText(bundle.getString("etunimi"));
		lastname1.setText(bundle.getString("sukunimi"));
		phone1.setText(bundle.getString("puhelinnumero"));
		email1.setText(bundle.getString("sahkoposti"));
		adress1.setText(bundle.getString("osoite"));
		peruuta.setText(bundle.getString("button_peruuta"));
		paivita.setText(bundle.getString("button_refresh"));
		muokkaa.setText(bundle.getString("button_edit"));
		omattiedot.setText(bundle.getString("omattiedot"));
		try {

			tarkistakukaonkirjautunutid();
			Connection con = AccessRegoTietokanta.getConnection(); // otetaan yhteys databaseen
			System.out.println("Olen initialize OmattiedotController printti ja tarkistan mikä on id " + id);

			String sql = "select * from " + database + ".omattiedot where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			resultSet = ps.executeQuery();

			while (resultSet.next()) {
				// haetaan databasesta näytettävät tiedot
				name.setText(resultSet.getString("etunimi"));
				lastname.setText(resultSet.getString("sukunimi"));
				phone.setText(resultSet.getString("puh"));
				email.setText(resultSet.getString("spost"));
				adress.setText(resultSet.getString("posti"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi jolla avataan omien tietojen muokkaus näkymä.
	 * @return
	 */
	public boolean showOmattiedotMuokkausView() {
		try {
			// metodi jolla näytetään omat tiedot
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/fxml/omattiedot_muokkaus_view.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Rego!"); //tästä otettu pois Omat tiedot muokkaus ja laitettu Rego! tilalle jotta ei tule ongelmia käännöksen kanssa
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage); // primary stage
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			OmattiedotMuokkausController controller = loader.getController();
			controller.setDialogStage(dialogStage);

			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
	 * Metodi joka tarkistaa kirjautujan id:n. Tieto haetaan tietokannasta.
	 */
	public void tarkistakukaonkirjautunutid() {

		try {
			Connection con2 = AccessRegoTietokanta.getConnection();
			String sql2 = "select * from rego.data where id=5";
			PreparedStatement ps2 = con2.prepareStatement(sql2);
			resultSet = ps2.executeQuery();

			while (resultSet.next()) {
				id = resultSet.getInt("data");
				System.out.print("OmattiedotController tarkistaa kuka on kirjautuneena " + id);
			}
			;
			con2.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Metodi joka tarkistaa kirjautujan aseman yrityksessä (työntekijä vai työnantaja). Tieto haetaan tietokannasta.
	 */
	public void tarkistakukaonkirjautunut() { // musta tuntuu että tämä on ylimääräinen
		try {
			Connection con1 = AccessRegoTietokanta.getConnection();
			String sql1 = "select * from rego.data where id=4";
			PreparedStatement ps1 = con1.prepareStatement(sql1);
			resultSet = ps1.executeQuery();

			while (resultSet.next()) {
				kuka = resultSet.getString("data");
				System.out.print("OmattiedotController tarkistaa kuka on kirjautuneena " + kuka);
			}
			;
			con1.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Metodi joka päivittää omat tiedot näkymän.
	 */
	@FXML
	private void handleRefresh() {
		try {
			Connection con1 = AccessRegoTietokanta.getConnection();
			String sql = "select * from " + database + ".omattiedot where id=?";
			PreparedStatement ps = con1.prepareStatement(sql);
			ps.setInt(1, id);
			resultSet = ps.executeQuery();
			while (resultSet.next()) {
				name.setText(resultSet.getString("etunimi"));
				lastname.setText(resultSet.getString("sukunimi"));
				phone.setText(resultSet.getString("puh"));
				email.setText(resultSet.getString("spost"));
				adress.setText(resultSet.getString("posti"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodi joka avaa muokkaaus ikkunan, jos käyttäjä on työnantaja.
	 */
	@FXML
	private void handleMuokkaa() {
		tarkistakukaonkirjautunut();
		if (kuka.equals("boss")) {
			showOmattiedotMuokkausView();
		} else {
			System.out.print("hahaa");
			// MINNI TÄHÄN JOKU POPUP ERROR MESSAGE "Tietoja voi muokata ainoastaan
			// Työnantaja, ota yhteyttä työnantajaan"
		}

	}

	/**
	 * Metodi asettaa dialogstagen.
	 * @param dialogStage
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * Metodi jota kutsutaan kun Cancel painetaan, jolla suljetaan omat tiedot ikkuna.
	 */
	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

	/**
	 * Metodi joka tarkistaa onko Ok nappia painettu
	*/
	
	public boolean isOkClicked() {
		return okClicked;
	}
}
