package com.ryhma_9.rego_view;

import java.util.ResourceBundle;

public interface Controller {
	
	/**
	 * Metodi joka vastaa kieli asetuksista
	*/
	
    void loadLanguage(String lang1, String lang2);
    ResourceBundle getYourBundle();
    String getLang1();
    String getLang2();
    
}
