package com.ryhma_9.rego_util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class AccessRegoTietokanta {

	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	final private String host = "10.114.32.34:3306";
	final private String user = "rego";
	final private String passwd = "Rego2020";
	final private String database = "rego";

	/**
	 * Metodi hoitaa yhteyden oton tietokantaan
	 */

	public void connectToDB() throws Exception {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			connect = DriverManager.getConnection(
					"jdbc:mysql://" + host + "/" + database + "?" + "user=" + user + "&password=" + passwd);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Metodi hoitaa yhteyden oton tietokantaan
	 */

	public static Connection getConnection() throws SQLException {
		Connection connection = DriverManager.getConnection("jdbc:mysql://" + "10.114.32.34:3306" + "/" + "rego" + "?"
				+ "user=" + "rego" + "&password=" + "Rego2020");

		return connection;
	}

	/**
	 * Metodi hoitaa tietokannan lukemisen
	 */

	public void readAnkkalinna() throws Exception {
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("select * from " + database + ".tunnukset");
			while (resultSet.next()) {
				int Id = resultSet.getInt("Id");
				String tunnus = resultSet.getString("tunnus");
				String salasana = resultSet.getString("salasana");

				System.out.println(String.format("Id: %d tunnus: %5s  salasana: %5s", Id, tunnus, salasana));
			}
		} catch (Exception e) { // HUOM! Dokumentti 5-OLSO
			throw e;
		}
	}

	/**
	 * Metodi hoitaa Tyotejtavat taulukon lukemisen
	 */

	public void readTyotehtavat() throws Exception {
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("select * from " + database + ".tyotehtavat");
			while (resultSet.next()) {
				int Id = resultSet.getInt("Id");
				String ttnimi = resultSet.getString("ttnimi");
				String klo = resultSet.getString("klo");
				String pvm = resultSet.getString("pvm");
				String animi = resultSet.getString("animi");
				int apuh = resultSet.getInt("apuh");
				String aosoite = resultSet.getString("aosoite");
				String ttieto = resultSet.getString("ttieto");

				System.out.println(String.format(
						"Id: %d ttnimi: %5s  klo: %5s pvm: %5s animi: %5s apuh: %d aosoite: %5s ttieto: %5s ", Id,
						ttnimi, klo, pvm, animi, apuh, aosoite, ttieto));
				Tyotehtava jaffa = new Tyotehtava(ttnimi, pvm);
				jaffa.getTtnimi();
			}
		} catch (Exception e) { // HUOM! Dokumentti 5-OLSO
			throw e;
		}
	}

	/**
	 * Metodi hoitaa yhteyden sulkemisen
	 */

	public void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				statement.close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}
}
