package com.ryhma_9.rego_util;

import java.time.LocalDate;


public class Tyotehtava {

	private String ttnimi;
	private String klo;
	private String pvm;
	private String animi;
	private Integer apuh;
	private String aosoite;
	private String ttieto;

	/**
	 * Tyhjä konstruktori
	 */
	public Tyotehtava() {
		this(null, null);
	}

	/**
	 * Konstruktori joka vaatii työtehtävän nimen sekä päivämäärän
	 * @param ttnimi
	 * @param pvm
	 */
	public Tyotehtava(String ttnimi, String pvm) {
		this.ttnimi = ttnimi;
		this.pvm = pvm;
	}

	public void setTtnimi(String ttnimi) {
		this.ttnimi = ttnimi;
	}

	public void setKlo(String klo) {
		this.klo = klo;
	}

	public void setPvm(String pvm) {
		this.pvm = pvm;
	}

	public void setAnimi(String animi) {
		this.animi = animi;
	}

	public void setApuh(Integer apuh) {
		this.apuh = apuh;
	}

	public void setAosoite(String aosoite) {
		this.aosoite = aosoite;
	}

	public void setTtieto(String ttieto) {
		this.ttieto = ttieto;
	}

	public String getTtnimi() {
		return ttnimi;
	}

	public String getKlo() {
		return klo;
	}

	public String getPvm() {
		return pvm;
	}

	public String getAnimi() {
		return animi;
	}

	public Integer getApuh() {
		return apuh;
	}

	public String getAosoite() {
		return aosoite;
	}

	public String getTtieto() {
		return ttieto;
	}
}
