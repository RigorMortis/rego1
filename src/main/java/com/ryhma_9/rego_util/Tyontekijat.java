package com.ryhma_9.rego_util;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Tyontekijat {

	private String etunimi;
	
	private String sukunimi;
	
	private String puh;
	
	private String spost;
	
	private String posti;
	
	private String id;
	

	/**
	 * Tyhjä konstruktori
	 */
	public Tyontekijat() {
		this(null, null, null, null, null, null);
	}

	/**
	 * Konstruktori jossa asetetaan kaikki työntekijä olion tiedot.
	 * @param id
	 * @param etunimi
	 * @param sukunimi
	 * @param puh
	 * @param spost
	 * @param posti
	 */
	public Tyontekijat(String id, String etunimi, String sukunimi, String puh, String spost, String posti) {
		
		this.etunimi = etunimi;
		this.sukunimi = sukunimi;
		this.puh = puh;
		this.spost = spost;
		this.posti = posti;
		this.id = id;
		
	}

	/**
	 * Etunimen getteri
	 * @return etunimi
	 */
	public String getEtunimi() {
		return etunimi;
	}

	/**
	 * Etunimen setteri
	 * @param etunimi
	 */
	public void setEtunimi(String etunimi) {
		this.etunimi = etunimi;
	}

	/**
	 * Sukunimen getteri
	 * @return sukunimi
	 */
	public String getSukunimi() {
		return sukunimi;
	}
 
	/**
	 * Sukunimi setteri
	 * @param sukunimi
	 */
	public void setSukunimi(String sukunimi) {
		this.sukunimi = sukunimi;
	}

	/**
	 * Puh getteri
	 * @return puh
	 */
	public String getPuh() {
		return puh;
	}
 
	/**
	 * puh setteri
	 * @param puh
	 */
	public void setPuh(String puh) {
		this.puh = puh;
	}

	/**
	 * Spost getteri
	 * @return spost
	 */
	public String getSpost() {
		return spost;
	}

	/**
	 * Spost setteri
	 * @param spost
	 */
	public void setSpost(String spost) {
		this.spost = spost;
	}
 
	/**
	 * Postin getteri
	 * @return posti
	 */
	public String getPosti() {
		return posti;
	}

	/**
	 * Postin setteri
	 * @param posti
	 */
	public void setPosti(String posti) {
		this.posti = posti;
	}

	/**
	 * ID:n getteri
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID:n setteri
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}	
}
