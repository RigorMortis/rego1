package com.ryhma_9.rego;

import java.io.IOException;

import com.ryhma_9.rego_util.AccessRegoTietokanta;
import com.ryhma_9.rego_view.KirjautuminenController;
import com.ryhma_9.rego_view.OmatTyotehtavatController;
import com.ryhma_9.rego_view.OmattiedotController;
import com.ryhma_9.rego_view.TyonantajaController;
import com.ryhma_9.rego_view.TyontekijaController;
import com.ryhma_9.rego_view.TyontekijatController;
import com.ryhma_9.rego_view.TyotehtavaController;
import com.ryhma_9.rego_view.TyotehtavaTarkasteluController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Main extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	AnchorPane page;
	FXMLLoader loaderi;

	/**
	 * Metodi vastaa ohjelman käynnistyksestä
	 */

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Rego!");

		// root layout on kehikko mille ei vielä ole käyttöä tällä hetkellä
		initRootLayout();

		// eka näkymä on kirjautuminen
		showKirjautuminenView();
	}

	/**
	 * Metodi käynnistää RootLayoutin
	 */

	public void initRootLayout() {
		try {

			// Ladataan rootlayout fxml tieodstosta
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/fxml/rootlayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			// näytetään scene jossa on root layout
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Testi load overview
	 */

	public FXMLLoader loadOverview(String getResource) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource(getResource));
			AnchorPane anchorpane = (AnchorPane) loader.load();

			// Set person overview into the center of root layout.
			rootLayout.setCenter(anchorpane);
			return loader;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Testi dialogStage
	 */

	public Stage createDialogStage(String resource, String title) {
		try {
			loaderi = new FXMLLoader();
			// parametrina source
			loaderi.setLocation(Main.class.getResource(resource));
			page = (AnchorPane) loaderi.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Rego!"); // jos joku menee rikki niin vaiha sulkuihin --title--
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			return dialogStage;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Metodi joka näyttää työnantajan näkymän
	 */
	public void showTyonantajaView() {
		FXMLLoader loader = loadOverview("/fxml/tyonantaja_view.fxml");
		TyonantajaController controller = loader.getController();
		controller.setMainApp(this);
	}

	/**
	 * Metodi joka näyttää työntekijän näkymän
	 */
	public void showTyontekijaView() {
		FXMLLoader loader = loadOverview("/fxml/tyontekija_view.fxml");
		TyontekijaController controller = loader.getController();
		controller.setMainApp(this);
	}

	/**
	 * Metodi joka näyttää kirjautumis näkymän
	 */
	public void showKirjautuminenView() {
		FXMLLoader loader = loadOverview("/fxml/kirjautuminen_view.fxml");
		KirjautuminenController controller = loader.getController();
		controller.setMeinApp(this);
	}

	/**
	 * Metodi joka näyttää työnantajantyötehtävä näkymän
	 */
	public boolean showTyotehtavaView() {
		Stage dialogStage = createDialogStage("/fxml/tyotehtava_view.fxml", "Tyotehtavat");
		TyotehtavaController controller = loaderi.getController();
		controller.setDialogStage(dialogStage);

		dialogStage.showAndWait();

		return controller.isOkClicked();
	}

	/**
	 * Metodi joka näyttää työtehtävätarkastelu näkymän
	 */
	public boolean showTyotehtavaTarkasteluView() {
		Stage dialogStage = createDialogStage("/fxml/tyotehtava_tarkastelu_view.fxml", "Tyotehtavat");

		TyotehtavaTarkasteluController controller = loaderi.getController();
		controller.setDialogStage(dialogStage);

		dialogStage.showAndWait();

		return controller.isOkClicked();
	}

	/**
	 * Metodi joka näyttää omattiedot näkymän
	 */
	public boolean showOmattiedotView() {

		Stage dialogStage = createDialogStage("/fxml/omattiedot_view.fxml", "Omat tiedot");

		OmattiedotController controller = loaderi.getController();
		controller.setDialogStage(dialogStage);

		dialogStage.showAndWait();

		return controller.isOkClicked();
	}

	/**
	 * Metodi joka näyttää työntekijän näkymän
	 */
	public boolean showTyontekijatView() {

		Stage dialogStage = createDialogStage("/fxml/tyontekijat_view.fxml", "Työntekijät");

		TyontekijatController controller = loaderi.getController();
		controller.setDialogStage(dialogStage);

		dialogStage.showAndWait();

		return controller.isOkClicked();

	}

	/**
	 * Metodi joka näyttää OmaTyotehtava näkymän
	 */
	public boolean showOmaTyotehtavaView() {

		Stage dialogStage = createDialogStage("/fxml/oma_tt_view.fxml", "Oma työtehtävä");

		OmatTyotehtavatController controller = loaderi.getController();
		controller.setDialogStage(dialogStage);

		dialogStage.showAndWait();

		return controller.isOkClicked();

	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
	 * Main metodi joka ottaa yhteyden tietokantaan ja lataa listaan työtehtävät,
	 * sekä käynnistää koko ohjelman.
	 */
	public static void main(String[] args) throws Exception {

		AccessRegoTietokanta db = new AccessRegoTietokanta();
		db.connectToDB();
		db.readAnkkalinna();
		db.readTyotehtavat();
		db.close();
		launch(args);
	}
}
