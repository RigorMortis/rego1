package Ryhma_9.Rego;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ryhma_9.rego_util.Tyotehtava;

/**
 * Unit testi tyotehtavan metodeille
 */
public class TyotehtavaTest {

	    private Tyotehtava task = new Tyotehtava();
	    private String nimi = "Matti Kukkaro";
		private String klo = "20:20:20";
		private String pvm = "13.12.2020";
		private String animi = "Dmitri Ylimaa";
		private Integer apuh = 04034534534;
		private String aosoite = "Peltotie 3";
		private String ttieto = "Ei lisättävää";
	    
	    @Test
	    public void testTyotehtava() {
	    	 task.setTtnimi(nimi);
	    	 task.setKlo(klo);
	    	 task.setPvm(pvm);
	    	 task.setAnimi(animi);
	    	 task.setApuh(apuh);
	    	 task.setAosoite(aosoite);
	    	 task.setTtieto(ttieto);
	    	 System.out.println("Työtehtävä: " + task.getTtnimi() + ", Asiakas: " + task.getAnimi());
	    	 assertEquals(nimi, task.getTtnimi());
	    	 assertEquals(klo, task.getKlo());
	    	 assertEquals(pvm, task.getPvm());
	    	 assertEquals(animi, task.getAnimi());
	    	 assertEquals(apuh, task.getApuh());
	    	 assertEquals(aosoite, task.getAosoite());
	    	 assertEquals(ttieto, task.getTtieto());
	    }  
}
