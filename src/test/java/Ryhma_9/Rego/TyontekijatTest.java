package Ryhma_9.Rego;

import static org.junit.Assert.*;

import org.junit.Test;
import com.ryhma_9.rego_util.Tyontekijat;

public class TyontekijatTest {

	@Test
	public void testTyontekijat() {
		String etunimi="Minni";
		String sukunimi="Oja";
		String puh="04055446655";
		String spost="mi@mi.fi";
		String posti="Adressikuja 3";
		String id="020202";
		Tyontekijat tt = new Tyontekijat();
		
		tt.setEtunimi(etunimi);
		tt.setSukunimi(sukunimi);
		tt.setPuh(puh);
		tt.setSpost(spost);
		tt.setPosti(posti);
		tt.setId(id);
		
		System.out.println("Nimi: " + tt.getEtunimi() + ", id: " + tt.getId());
		assertEquals(etunimi, tt.getEtunimi());
		assertEquals(sukunimi, tt.getSukunimi());
		assertEquals(puh, tt.getPuh());
		assertEquals(spost, tt.getSpost());
		assertEquals(posti, tt.getPosti());
		assertEquals(id, tt.getId());
	}

}
