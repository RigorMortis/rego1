package Ryhma_9.Rego;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import com.ryhma_9.rego_util.AccessRegoTietokanta;
import com.ryhma_9.rego_util.Tyotehtava;


public class AccessRegoDBTest {
	private ResultSet resultSet = null;	
	private PreparedStatement ps = null;
	final private String database = "rego";
	private Connection con = null;
	
	private String name="Menni";
	private String lastname="Test";
	private Integer phone=055334455;
	private String email="m@m.com";
	private String adress="Hki";
	
	
	@Test
	public void testConnection() throws SQLException {
		AccessRegoTietokanta db = new AccessRegoTietokanta();
		try {
			
			db.connectToDB();
			assertNotNull(db);
			
			assertNull(con);
			con = AccessRegoTietokanta.getConnection();
			assertNotNull(con);
			
			db.readAnkkalinna();
			db.readTyotehtavat();
			db.close();
		} catch (Exception e) {
		}
	}
	
}
