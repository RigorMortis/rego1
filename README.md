HUOM!  

- **mysql connectorin saa ladattuun ** https://dev.mysql.com/downloads/connector/j/
- **openjfx-11.0.2_windows-x64_bin-sdk saa ladattuun** https://gluonhq.com/products/javafx/  josta **JavaFX Windows SDK**

1. Asenna Eclipse, luo Gitlab tunnukset ja varmista että sinulla on oikeudet Rego1 projektiin.
2. Luo uusi workspace Eclipseä käynnistäessä, sen jälkeen vasemmasta laidasta valitse "import projects... >>> Git >>> Projects from Git (with smart import) >>> Clone URL"
3. Sijoita "URL" riville https://gitlab.com/TheSovietTerror/rego1, täytä muut tarvittavat kohdat ja paina "next" >>> next >>> next >>> finish
4. Nyt sinulla pitäisi näkyä projekti näkymässä, vasemassa laidassa "Rego". Paina oikealla hiiren klikkausella projektia >>> build path >>> configure build path.
5. Libraries tabissä valitse "Add external Jar" ja lisää "mysql-connector-java-8.0.21.jar", sitten paina "Apply"
6. Libraries tabissa valitse "Add library" >>> "User library" >>> "User libraries... (oikeassa laidassa)" >>> "New" >>> anna kirjaston nimeksi JavaFX ja paina "ok" >>> "add external JARs" >>> lisäät sinne "openjfx-11.0.2_windows-x64_bin-sdk >>> javafx-sdk-11.0.2 >>> lib" ja sieltä kaikki JAR tiedostot. Ja paina apply and close niin kauan kunnes pääset takaisin perus eclipsen näkymään. 
7. Klikkaat oikealla "Main.java" luokkaa, sitten viet hiiren "Run As" kohdalle ja sieltä valitset "Run Configurations..." sitten menet "Arguments tabiin" ja "VM arguments" kenttään laitat "--module-path /path/to/JavaFX/lib --add-modules=javafx.controls,javafx.fxml" >>> HUOM (/path/to/JavaFX/lib vaihdat omaan tiedosto polkuun)
8. Muista VPN yhteys koulun verkkoon!
9. Tämän jälkeen kun suoritat "Main.java" tiedoston niin näkyville pitäisi ilmestyä kirjautumisnäkymä